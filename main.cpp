#include <iostream>
#include <vector>

using namespace std;

int getRandomInt(int min, int max) {
    return (rand() % (max - min + 1)) + min;
}

template<class T>
void printVec(vector<T> &v) {
    if (v.empty()) return;
    cout << "{";
    for (int i = 0; i < v.size(); i++) {
        cout << v[i];
        if (i != v.size() - 1) cout << ", ";
    }
    cout << "}";
}

template<class T>
void insertion_sort(vector<T> &array) {
    for (int i = 1; i < array.size(); i++) {
        T tmp = array[i];
        int j = i - 1;
        while (array[j] > tmp && j >= 0) {
            array[j + 1] = array[j];
            j--;
        }
        array[j + 1] = tmp;
    }
}

template<class T>
void selection_sort(vector<T> &array) {
    for (int i = 0; i < array.size(); i++) {
        T tmp = array[i];
        int smallestIndex = i;
        for (int j = i + 1; j < array.size(); j++) {
            if (tmp > array[j]) {
                tmp = array[j];
                smallestIndex = j;
            }
        }
        array[smallestIndex] = array[i];
        array[i] = tmp;
    }
}

template<class T>
void bubble_sort(vector<T> &array) {
    for (int i = 0; i < array.size() - 1; i++) {
        for (int j = 1; j < array.size() - i; j++) {
            if (array[j] < array[j - 1]) {
                T tmp = array[j];
                array[j] = array[j - 1];
                array[j - 1] = tmp;
            }
        }
    }
}

template<class T>
void quick_sort(vector<T> &array) {
    if (array.size() < 2) return;
    T pivot = array[getRandomInt(0, array.size() - 1)];
    vector<T> left, right, pivots;

    for (int i = 0; i < array.size(); i++) {
        if (array[i] < pivot)
            left.push_back(array[i]);
        else if (array[i] > pivot)
            right.push_back(array[i]);
        else
            pivots.push_back(array[i]);
    }

    quick_sort(left);
    quick_sort(right);

    array.clear();

    array.insert(array.end(), left.begin(), left.end());
    array.insert(array.end(), pivots.begin(), pivots.end());
    array.insert(array.end(), right.begin(), right.end());
}

template<class T>
void merge_sort(vector<T> &array, int start, int end) {

    if (start == end) return;

    int split = (start + end) / 2;

    merge_sort(array, start, split);
    merge_sort(array, split + 1, end);

    for (int i = 0; i < split + 1; i++) {
        if (array[i] > array[split + 1]) {
            T tmp = array[i];
            array[i] = array[split + 1];
            array[split + 1] = tmp;
        }
    }
}

template<class T>
void merge_sort(vector<T> &array) {
    merge_sort(array, 0, array.size() - 1);
}

int main() {
    cout << endl;
    vector<int> array;
    srand(time(NULL));

    for (int i = 0; i < 20000; i++) {
        array.push_back(getRandomInt(-1000000, 100000));
    }

    printVec(array);
    cout << endl;

    vector<int> insertion_sort_array = array;
    insertion_sort(insertion_sort_array);
    cout << "Insertion sort: ";
    printVec(insertion_sort_array);
    cout << endl;

    vector<int> selection_sort_array = array;
    selection_sort(selection_sort_array);
    cout << "Selection sort: ";
    printVec(selection_sort_array);
    cout << endl;

    vector<int> bubble_sort_array = array;
    bubble_sort(bubble_sort_array);
    cout << "Bubble sort:    ";
    printVec(bubble_sort_array);
    cout << endl;

    vector<int> quick_sort_array = array;
    quick_sort(quick_sort_array);
    cout << "Quick sort:     ";
    printVec(quick_sort_array);
    cout << endl;

    vector<int> merge_sort_array = array;
    merge_sort(merge_sort_array);
    cout << "Merge sort:     ";
    printVec(merge_sort_array);
    cout << endl;


    if (insertion_sort_array == selection_sort_array && selection_sort_array == bubble_sort_array &&
        bubble_sort_array == quick_sort_array && quick_sort_array == merge_sort_array) {
        cout << "Works!";
    } else {
        cout << "NOOOO!!!!!";
    }

    return 0;
}
